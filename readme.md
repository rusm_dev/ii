# Api

| Route                       | Params\body       | Return                                                                       |
|-----------------------------|-------------------|------------------------------------------------------------------------------|
| api/users/:telegramId [get] | 123               | {telegramId, id}                                                             |
| api/users/ [post]           | {telegramId: 123} | {telegramId, id}                                                             |
| api/brief/day/:date [get]   | 2022-07-27        | {amount, area, inActionAmount, inActionArea, localizedAmount, localizedArea} |
| api/brief/month/:date [get] | 12.2022           | {amount, area, inActionAmount, inActionArea, localizedAmount, localizedArea} |