import fs from "fs";

const dynamicResp = JSON.parse(fs.readFileSync('data/yasen_07_2022_getDynamicsResponse.json', 'utf8'))[0].Body.getDynamicsResponse.MessageData.AppData.data // =>
// "fireIdField": "f88ff892-3991-4d32-8e86-d6412036d5b6", сквозной (по всем 4м файлам-методам) уникальный идентификатор пожара
// "detection": "18.06.2022 16:50", дата время изменения статуса
// state": "Продолжается", статус пожара
// "type": "Низовой устойчивый", тип пожара, в Га
// "intensity": "Средняя   ", интенсивность
// "plants": "Хвойные насаждения", вид растительности
// "mainSpecies": "Ель", основная порода
// "forest": 0.1, площадь пожара


const fireResp = JSON.parse(fs.readFileSync('data/yasen_07_2022_getFireInformationResponse.json', 'utf8'))[0].Body.getFireInformationResponse.MessageData.AppData.data // =>
// "fireIdField": "1178fff9-e942-4b72-b5d4-adac5eaf4534", сквозной (по всем 4м файлам) уникальный идентификатор пожара
// "aviation": "Березовское авиаотделение", Авиаотделение
// "forest": "Берёзовское", Лесничество
// "divisionalForest": "Саранпаульское", Участковое лесничество
// "forestFireNumber": 1, номер пожара по лесничеству
// "aviationFireNumber": 1, номер пожара по авиаотделению
// "forestFundHolder": "Земли лесного фонда", Лесной фонд
// "monitoringZone": "Авиационная (район АСС)", Зона мониторинга
// "latitude": { координаты широты wgs84
//     "degree": 64,
//         "minute": 2,
//         "second": 46
// },
// "longitude": { координаты долготы wgs84
//     "degree": 63,
//         "minute": 52,
//         "second": 40
// },
// "municipality": "д Щекурья", муниципальное образование
// "extinctionTime": "11.06.2022 19:10", время обнаружения
// "groundCover": "Зеленомошный", тип лесного покрова
// "categoryForest": "Эксплуатационные", категория лесов
// "categoryLand": "Насаждения естеств.происхожд.", насаждений
// "cause": "от гроз", причина возникновения пожара
// "detectionMethod": "Воздушные суда", способ обнаружения


const peopleResp = JSON.parse(fs.readFileSync('data/yasen_07_2022_getInvolvedPeopleResponse.json', 'utf8'))[0].Body.getInvolvedPeopleResponse.MessageData.AppData.data // =>
// "fireIdField": "1178fff9-e942-4b72-b5d4-adac5eaf4534", сквозной (по всем 4м файлам) уникальный идентификатор пожара
// "tip": "Десантные пожарные команды", тип привлекаемых к тушению сил
// "delivered": 11, кол-во привлеченных сил


const techniqueResp = JSON.parse(fs.readFileSync('data/yasen_07_2022_getInvolvedTechniqueResponse.json', 'utf8'))[0].Body.getInvolvedTechniqueResponse.MessageData.AppData.data // =>
// "fireIdField": "1178fff9-e942-4b72-b5d4-adac5eaf4534",  cквозной (по всем 4м файлам) уникальный идентификатор пожара
// "tip": "Ручные орудия", тип применяемых средств тушения
// "count": 12, кол-во тип применяемых средств тушения

import { PrismaClient } from '@prisma/client'
import { faker } from "@faker-js/faker";

const prisma = new PrismaClient()

interface People {
    fireIdField: string
    tip: string
    delivered: number
}

interface Technique {
    fireIdField: string
    tip: string
    count: number
}

interface Fire {
    fireIdField: string
    aviation: string
    forest: string
    divisionalForest: string
    forestFireNumber: number
    aviationFireNumber: number
    forestFundHolder: string
    monitoringZone: string
    latitude: {
        degree: number
        minute: number
        second: number
    }
    longitude: {
        degree: number
        minute: number
        second: number
    }
    municipality: string
    extinctionTime: string
    groundCover: string
    categoryForest: string
    categoryLand: string
    cause: string
    detectionMethod: string
}


interface FireFire {
    fireIdField: string
    aviation: string
    forest: string
    divisionalForest: string
    forestFireNumber: number
    aviationFireNumber: number
    forestFundHolder: string
    monitoringZone: string
    latitude: {
        degree: number
        minute: number
        second: number
    }
    longitude: {
        degree: number
        minute: number
        second: number
    }
    municipality: string
    extinctionTime: string
    groundCover: string
    categoryForest: string
    categoryLand: string
    cause: string
    detectionMethod: string
}

interface FireStatic {
    fireIdField: string
    detection: string
    state: string
    type: string
    intensity: string
    plants: string
    mainSpecies: string
    forest: number
}


interface City {
    name: string
    lat: number
    lon: number
}

const cities: City[] = [
    {
        name: 'с Чеускино',
        lat: 61.122174,
        lon: 72.435358
    }, {
        name: 'с Перегребное',
        lat: 62.967493,
        lon: 65.085718
    }, {
        name: 'с Ванзеват',
        lat: 64.233680,
        lon: 66.036217
    }, {
        name: 'с Былино',
        lat: 60.744139,
        lon: 76.815784
    }, {
        name: 'с Болчары',
        lat: 59.814238,
        lon: 68.811705
    }, {
        name: 'п Юбилейный',
        lat: 61.186591,
        lon: 62.776733
    }, {
        name: 'д Щекурья',
        lat: 64.270698,
        lon: 60.854887
    }, {
        name: 'д Скрипунова',
        lat: 61.327006,
        lon: 69.756751
    }, {
        name: 'д Верхне-Мысовая',
        lat: 61.113718,
        lon: 75.123843
    }
]

const addCities = async (cities: City[]) => {
    const citiesInDb = await prisma.city.createMany({
        data: cities.map(city => ({
            name: city.name,
            lat: city.lat,
            lon: city.lon,
            personal: faker.datatype.number({min: 150, max: 400}),
        }))
    })
}

addCities(cities)


dynamicResp.map(async (item: FireStatic, index: number) => {
    const rawDate = item.detection
    const date = new Date(`${rawDate.split(' ')[0].split('.')[2]}-${rawDate.split(' ')[0].split('.')[1]}-${rawDate.split(' ')[0].split('.')[0]} ${rawDate.split(' ')[1]} +0000`)
    let fire = await prisma.fire.upsert({
        where: {
            fireIdField: item.fireIdField
        },
        update: {
            fireIdField: item.fireIdField,
            detection: date,
            state: item.state,
            type: item.type,
            intensity: item.intensity,
            plants: item.plants,
            mainSpecies: item.mainSpecies,
            forest: item.forest,
        },
        create: {
            fireIdField: item.fireIdField,
            detection: date,
            state: item.state,
            type: item.type,
            intensity: item.intensity,
            plants: item.plants,
            mainSpecies: item.mainSpecies,
            forest: item.forest,
        }
    })
})

fireResp.map(async (item: FireFire, index: number) => {
    console.log(index, item)
    let fire = await prisma.fire.upsert({
        where: {
            fireIdField: item.fireIdField,
        },
        update: {
            fireIdField: item.fireIdField,
            aviation: item.aviation,
            forestName: item.forest,
            divisionalForest: item.divisionalForest,
            forestFireNumber: item.forestFireNumber,
            aviationFireNumber: item.aviationFireNumber,
            forestFundHolder: item.forestFundHolder,
            monitoringZone: item.monitoringZone,
            latitude: `${item.latitude.degree}.${item.latitude.minute}.${item.latitude.second}`,
            longitude: `${item.longitude.degree}.${item.longitude.minute}.${item.longitude.second}`,
            municipality: item.municipality,
            extinctionTime: item.extinctionTime,
            groundCover: item.groundCover,
            categoryForest: item.categoryForest,
            categoryLand: item.categoryLand,
            cause: item.cause,
            detectionMethod: item.detectionMethod,
        },
        create: {
            fireIdField: item.fireIdField,
            aviation: item.aviation,
            forestName: item.forest,
            divisionalForest: item.divisionalForest,
            forestFireNumber: item.forestFireNumber,
            aviationFireNumber: item.aviationFireNumber,
            forestFundHolder: item.forestFundHolder,
            monitoringZone: item.monitoringZone,
            latitude: `${item.latitude.degree}.${item.latitude.minute}.${item.latitude.second}`,
            longitude: `${item.longitude.degree}.${item.longitude.minute}.${item.longitude.second}`,
            municipality: item.municipality,
            extinctionTime: item.extinctionTime,
            groundCover: item.groundCover,
            categoryForest: item.categoryForest,
            categoryLand: item.categoryLand,
            cause: item.cause,
            detectionMethod: item.detectionMethod,
        }
    })
})

peopleResp.map(async (item: People, index: number) => {
    console.log(index, item)
    let fire = await prisma.firePerson.create({
        data: {
            fireFireIdField: item.fireIdField,
            PersonType: item.tip,
            PersonCount: item.delivered,
        }
    })
})

techniqueResp.map(async (item: Technique, index: number) => {
    console.log(index, item)
    let fire = await prisma.fireTechnique.create({
        data: {
            fireFireIdField: item.fireIdField,
            TechniqueType: item.tip,
            TechniqueCount: item.count,
        }
    })
})
