import fs from 'fs';
import { PrismaClient } from '@prisma/client';
import {generatePie, generateLine} from '../test3';
import dayjs from 'dayjs';

const prisma = new PrismaClient();

const dynamicResp = JSON.parse(fs.readFileSync('data/yasen_07_2022_getDynamicsResponse.json', 'utf8'))[0].Body.getDynamicsResponse.MessageData.AppData.data // =>
// "fireIdField": "f88ff892-3991-4d32-8e86-d6412036d5b6", сквозной (по всем 4м файлам-методам) уникальный идентификатор пожара
// "detection": "18.06.2022 16:50", дата время изменения статуса
// state": "Продолжается", статус пожара
// "type": "Низовой устойчивый", тип пожара, в Га
// "intensity": "Средняя   ", интенсивность
// "plants": "Хвойные насаждения", вид растительности
// "mainSpecies": "Ель", основная порода
// "forest": 0.1, площадь пожара


const fireResp = JSON.parse(fs.readFileSync('data/yasen_07_2022_getFireInformationResponse.json', 'utf8'))[0].Body.getFireInformationResponse.MessageData.AppData.data // =>
// "fireIdField": "1178fff9-e942-4b72-b5d4-adac5eaf4534", сквозной (по всем 4м файлам) уникальный идентификатор пожара
// "aviation": "Березовское авиаотделение", Авиаотделение
// "forest": "Берёзовское", Лесничество
// "divisionalForest": "Саранпаульское", Участковое лесничество
// "forestFireNumber": 1, номер пожара по лесничеству
// "aviationFireNumber": 1, номер пожара по авиаотделению
// "forestFundHolder": "Земли лесного фонда", Лесной фонд
// "monitoringZone": "Авиационная (район АСС)", Зона мониторинга
// "latitude": { координаты широты wgs84
//     "degree": 64,
//         "minute": 2,
//         "second": 46
// },
// "longitude": { координаты долготы wgs84
//     "degree": 63,
//         "minute": 52,
//         "second": 40
// },
// "municipality": "д Щекурья", муниципальное образование
// "extinctionTime": "11.06.2022 19:10", время обнаружения
// "groundCover": "Зеленомошный", тип лесного покрова
// "categoryForest": "Эксплуатационные", категория лесов
// "categoryLand": "Насаждения естеств.происхожд.", насаждений
// "cause": "от гроз", причина возникновения пожара
// "detectionMethod": "Воздушные суда", способ обнаружения


const peopleResp = JSON.parse(fs.readFileSync('data/yasen_07_2022_getInvolvedPeopleResponse.json', 'utf8'))[0].Body.getInvolvedPeopleResponse.MessageData.AppData.data // =>
// "fireIdField": "1178fff9-e942-4b72-b5d4-adac5eaf4534", сквозной (по всем 4м файлам) уникальный идентификатор пожара
// "tip": "Десантные пожарные команды", тип привлекаемых к тушению сил
// "delivered": 11, кол-во привлеченных сил


const techniqueResp = JSON.parse(fs.readFileSync('data/yasen_07_2022_getInvolvedTechniqueResponse.json', 'utf8'))[0].Body.getInvolvedTechniqueResponse.MessageData.AppData.data // =>
// "fireIdField": "1178fff9-e942-4b72-b5d4-adac5eaf4534",  cквозной (по всем 4м файлам) уникальный идентификатор пожара
// "tip": "Ручные орудия", тип применяемых средств тушения
// "count": 12, кол-во тип применяемых средств тушения
const getBrief = async (datetimeStart, datetimeEnd) => {
    const commonResult = await prisma.fire.aggregate(
        {
            where: {
                detection: {
                    gte: datetimeStart,
                    lt: datetimeEnd
                }
            },
            _sum: {
                forest: true
            },
            _count: true
        }
    )

    const inActionResult = await prisma.fire.aggregate({
        where: {
            detection: {
                gte: datetimeStart,
                lt: datetimeEnd
            },
            state: 'Продолжается'
        },
        _sum: {
            forest: true
        },
        _count: true
    })

    const localizedResult = await prisma.fire.aggregate({
        where: {
            detection: {
                gte: datetimeStart,
                lt: datetimeEnd
            },
            state: 'Локализован'
        },
        _sum: {
            forest: true
        },
        _count: true
    })

    return {
        amount: commonResult._count,
        area: Number(commonResult._sum.forest),
        inActionAmount: inActionResult._count,
        inActionArea: Number(inActionResult._sum.forest),
        localizedAmount: localizedResult._count,
        localizedArea: Number(localizedResult._sum.forest),
        datetimeStart,
        datetimeEnd
    }
}

export const weeklyBrief = async (day: string) => {
    const datetimeEnd = new Date(dayjs(day).add(5, 'hour').format('YYYY-MM-DD HH:mm:ss'))
    let datetimeStart = new Date(dayjs(day).subtract(7, 'day').add(5, 'hour').format('YYYY-MM-DD HH:mm:ss'))
    const result = await getBrief(datetimeStart, datetimeEnd)
    const imgs = await getFiresCause(datetimeStart, datetimeEnd)
    console.log(imgs)
    
    return {
        ...result,
        ...imgs
    }
}

export const monthlyBrief = async (day: string) => {
    const datetimeEnd = new Date(dayjs(day).add(5, 'hour').format('YYYY-MM-DD HH:mm:ss'))
    const datetimeStart = new Date(dayjs(day).subtract(1, 'month').add(5, 'hour').format('YYYY-MM-DD HH:mm:ss'))
    const result = await getBrief(datetimeStart, datetimeEnd)
    const imgs = await getFiresCause(datetimeStart, datetimeEnd)
    
    return {
        ...result,
        ...imgs
    }
}

export const seasonBrief = async () => {
    const datetimeStart = new Date(dayjs(dynamicResp[0].inputField.begin).add(5, 'hour').format('YYYY-MM-DD HH:mm:ss'))
    const datetimeEnd = new Date(dayjs(dynamicResp[0].inputField.end).add(5, 'hour').format('YYYY-MM-DD HH:mm:ss'))
    const result = await getBrief(datetimeStart, datetimeEnd)
    const imgs = await getFiresCause(datetimeStart, datetimeEnd)
    
    return {
        ...result,
        ...imgs
    }
}

export const dailyBrief = async (day: string) => {
    const datetimeStart = new Date(dayjs(day).add(5, 'hour').format('YYYY-MM-DD HH:mm:ss'))
    let datetimeEnd = new Date(dayjs(day).add(1, 'day').add(5, 'hour').format('YYYY-MM-DD HH:mm:ss'))
    const result = await getBrief(datetimeStart, datetimeEnd)
    const imgs = await getFiresCause(datetimeStart, datetimeEnd)
    
    return {
        ...result,
        ...imgs
    }
}

export const getFiresCause = async (datetimeStart, datetimeEnd) => {
    const causes = await prisma.fire.groupBy(
        {
            by: ['cause'],
            where: {
                detection: {
                    gte: datetimeStart,
                    lt: datetimeEnd
                }
            },
            _count: {
                cause: true
            }
        }
    )

    const types = await prisma.fire.groupBy(
        {
            by: ['type'],
            where: {
                detection: {
                    gte: datetimeStart,
                    lt: datetimeEnd
                }
            },
            _count: {
                type: true
            }
        }
    )

    
    return {
        causes: generatePie(causes.map(item => item._count.cause), causes.map(item => item.cause || 'Неизвестно')),
        types: generatePie(types.map(item => item._count.type), types.map(item => item.type || 'Неизвестно'))
    }
}

export const getDaylyStatistic = async (datetimeStart, datetimeEnd) => {

}

export const generateAmount = async (datetimeStart, datetimeEnd, type) => {

    const result = await prisma.fire.groupBy({
        by: [type],
        where: {
            detection: {
                gte: new Date(datetimeStart.subtract(1, 'day').format('YYYY-MM-DD HH:mm:ss')),
                lt: new Date(datetimeEnd)
            }
        },
        _count: true,
    })
    return generateLine(result.map(item => ({t: item[type], y: item._count})), type)
}

export const generateArea = async (datetimeStart, datetimeEnd, type) => {
    const result = await prisma.fire.groupBy({
        by: [type],
        where: {
            detection: {
                gte: new Date(datetimeStart.subtract(1, 'day').format('YYYY-MM-DD HH:mm:ss')),
                lt: new Date(datetimeEnd)
            }
        },
    })
    return generateLine(result.map(item => ({t: item[type], y: item.forest})), type)
}