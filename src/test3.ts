import { ChartJSNodeCanvas } from "chartjs-node-canvas";
import dayjs from "dayjs";

import fs from "fs";

const chartJSNodeCanvas = new ChartJSNodeCanvas({
  width: 900,
  height: 600,
  backgroundColour: "white",
});

export const generatePie = (data, labels) => {
  const image = chartJSNodeCanvas.renderToBufferSync({
    type: "doughnut",
    data: {
      datasets: [
        {
          label: "Причины возгораний",
          data: data,
          backgroundColor: [
            'rgb(255, 99, 132)',
            'rgb(54, 162, 235)',
            'rgb(12, 203, 120)',
            'rgb(85, 17, 112)',
            'rgb(255, 205, 86)'
          ],
        },
      ],
      labels: labels,
    },
    options: {
      plugins: {
          legend: {
              labels: {
                  // This more specific font property overrides the global property
                  font: {
                      size: 15
                  }
              }
          }
      }
  }
  });

  fs.writeFileSync("image.png", image, "binary");

  return image;
}

export const generateLine = () => {
  const image = chartJSNodeCanvas.renderToBufferSync({
    type: 'line',
    data: {
      labels: ['2022-11-30', '2022-12-01', '2022-12-02', '2022-12-03', '2022-12-04'],
      datasets: [{
        label: 'Колличество возгораний в период 2022-11-30 - 2022-12-04',
        data: [15.8, 32, 12, 19, 16, 22, 18],
        fill: false,
        borderColor: 'rgb(75, 192, 192)',
        tension: 0.1
      }]
    },
    options: {
      scales: {
        y: {
          suggestedMin: 0,
        }
      }
    }
  })

  fs.writeFileSync("image.png", image, "binary");

  return image;
}

generateLine()

