import express from 'express';
import { PrismaClient } from '@prisma/client'
import {dailyBrief, monthlyBrief, seasonBrief, weeklyBrief, generateAmount, generateArea, getFiresCause} from "./parse/parseData";
import { generateLine } from './test3';
import {generateData} from "./utils/generateApi";
import dayjs from 'dayjs';

const prisma = new PrismaClient()
const app = express();
app.use(express.json());

app.post('/api/users/', async (req, res) => {
    const user = await prisma.user.upsert({
        where: {
            telegramId: Number(req.body.telegramId)
        },
        update: {},
        create: {
            telegramId: Number(req.body.telegramId),
        }
    })
    return res.json(user)
})

app.get('/api/users/', async (req, res) => {
    const users = await prisma.user.findMany()
    return res.json(users)
})
app.get('/api/users/:telegramId', async (req, res) => {
    const user = await prisma.user.findUnique({
        where: {
            telegramId: Number(req.params.telegramId),
        },
    })
    return res.json(user || {})
})

app.get('/api/brief/day/:date', async (req, res) => {
    return res.json(await dailyBrief(req.params.date))
})

app.get('/api/brief/week/:date', async (req, res) => {
    return res.json(await weeklyBrief(req.params.date))
})

app.get('/api/brief/month/:date', async (req, res) => {
    return res.json(await monthlyBrief(req.params.date))
})

app.get('/api/brief/season', async (req, res) => {
    return res.json(await seasonBrief())
})

app.get('/api/latest', async (req, res) => {
    generateData().then(data => {
        return res.json(data)
    })
})

app.post('/api/statistics/', async (req, res) => {
    const datetimeStart = req.body.datetimeStart
    const datetimeEnd = req.body.datetimeEnd
    const categories: string[] = req.body.categories
    const dts = dayjs(datetimeStart)
    const dte = dayjs(datetimeEnd)
    const difference = dte.diff(dts, 'day')

    const graphs = await Promise.all(categories.map(async (category) => {
        if (category === 'amount') {
            generateLine()
        }
        if (category === 'area') {
            if (difference > 21) {
                return {data: await generateArea(dts, dte, 'day'), category}
            } else {
                return {data: await generateArea(dts, dte, 'week'), category}
            }
        }
        if (category === 'causes') {
            return {data: await getFiresCause(new Date(datetimeStart), new Date(datetimeEnd)), category}
        }
    }))

    console.log(graphs);
    
    return res.json(graphs)
})


app.listen(3000, () => {
    console.log('Listening on port 3000');
})