export const getDistance = (fire_lat: string, fire_lon: string, city_lat: number, city_lot: number) => {
    const R = 6371e3;
    // fire lat = 62.49.55 fire lon = 62.52.11
    const fire_lat_rad = Number(fire_lat.split('.')[0]) + Number(fire_lat.split('.')[1]) / 60 + Number(fire_lat.split('.')[2]) / 3600;
    const fire_lon_rad = Number(fire_lon.split('.')[0]) + Number(fire_lon.split('.')[1]) / 60 + Number(fire_lon.split('.')[2]) / 3600;
    const city_lat_rad = city_lat * Math.PI / 180;
    const city_lon_rad = city_lot * Math.PI / 180;
    const delta_lat = (city_lat_rad - fire_lat_rad) * Math.PI / 180;
    const delta_lon = (city_lon_rad - fire_lon_rad) * Math.PI / 180;
    // get distance in kilometers
    const a = Math.sin(delta_lat / 2) * Math.sin(delta_lat / 2) + Math.cos(fire_lat_rad * Math.PI / 180) * Math.cos(city_lat_rad) * Math.sin(delta_lon / 2) * Math.sin(delta_lon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c;
    console.log(d / 1000)
}
