import { faker } from '@faker-js/faker'
import { PrismaClient } from '@prisma/client'
import { getDistance } from './distance';

import dayjs from 'dayjs'
import weekOfYear from 'dayjs/plugin/weekOfYear'
dayjs.extend(weekOfYear)

const prisma = new PrismaClient()

faker.setLocale('ru')

const roundUpTo = roundTo => (x: number) => Math.ceil(x / roundTo) * roundTo;
const roundUpTo5Minutes = roundUpTo(1000 * 60 * 5);

export const generateData = async () => {
    const activeFires = await prisma.fire.findMany(
        {
            where: {
                detection: {
                    gte: new Date('2022-12-03'),
                    lt: new Date(Date.now() + 1000 * 60 * 60 * 5)
                },
                state: 'Продолжается'
            }
        }
    )

    const localizedFires = await prisma.fire.findMany(
        {
            where: {
                detection: {
                    gte: new Date('2022-12-03'),
                    lt: new Date(Date.now() + 1000 * 60 * 60 * 5)
                },
                state: 'Локализован'
            }
        }
    )
    if (activeFires.length > 0) {
        const fire = await activeFires[faker.datatype.number({ min: 0, max: activeFires.length - 1 })]
        const fireArea = Number(fire.forest) + faker.datatype.number({ min: 0, max: 3 })

        const newFire = await prisma.fire.update({
            where: {
                fireIdField: fire.fireIdField
            },
            data: {
                state: 'Локализован',
                detection: new Date(dayjs().add(5, 'hour').format('YYYY-MM-DD HH:mm:ss')),
                day: new Date(dayjs().add(5, 'hour').format('YYYY-MM-DD')),
                week: dayjs().add(5, 'hour').week(),
            }
        })

        return {
            location: fire.forestName,
            number: fire.forestFireNumber,
            state: "Локализирован",
            area: fireArea,
            datetimeEnd: dayjs().format('YYYY-MM-DD HH:mm:ss').toString(),
            datetimeStart: dayjs(fire.detection).subtract(5, 'hour').format('YYYY-MM-DD HH:mm:ss').toString(),
            cause: fire.cause,
            day: new Date(dayjs().add(5, 'hour').format('YYYY-MM-DD')),
            week: dayjs().add(5, 'hour').week(),
        }
    } else if (localizedFires.length > 0) {
        const fire = await localizedFires[faker.datatype.number({ min: 0, max: localizedFires.length - 1 })]
        const fireArea = Number(fire.forest) + faker.datatype.number({ min: 0, max: 0.5 })

        const newFire = await prisma.fire.update({
            where: {
                fireIdField: fire.fireIdField
            },
            data: {
                state: 'Ликвидирован',
                detection: new Date(dayjs().add(5, 'hour').format('YYYY-MM-DD HH:mm:ss')),
                day: new Date(dayjs().add(5, 'hour').format('YYYY-MM-DD')),
                week: dayjs().add(5, 'hour').week(),
                eliminated_time: dayjs().add(5, 'hour').diff(dayjs(fire.detection).format('YYYY-MM-DD HH:mm:ss'), 'minute')
            }
        })

        return {
            location: fire.forestName,
            number: fire.forestFireNumber,
            state: "Ликвидирован",
            area: fireArea,
            datetimeStart: dayjs(fire.detection).subtract(5, 'hour').format('YYYY-MM-DD HH:mm:ss').toString(),
            datetimeEnd: dayjs().format('YYYY-MM-DD HH:mm:ss').toString(),
            cause: fire.cause,
        }
    } else {
        const newFire = await prisma.fire.create({
            data: {
                fireIdField: faker.datatype.string(36),
                state: 'Продолжается',
                forest: faker.datatype.number({ min: 3, max: 30 }),
                latitude: faker.address.latitude(),
                longitude: faker.address.longitude(),
                forestName: faker.helpers.arrayElement(['Сургутское', 'Советское', 'Нижневартовское']),
                cause: faker.helpers.arrayElement(['от гроз', 'лесной пожар перешел с земель иных категорий', 'по вине населения']),  
                forestFireNumber: faker.datatype.number({ min: 1, max: 20 }),      
                type: faker.helpers.arrayElement(['Низовой беглый', 'Низовой устойчивый', null]),
                detection: new Date(dayjs().add(5, 'hour').format('YYYY-MM-DD HH:mm:ss')),
                day: new Date(dayjs().add(5, 'hour').format('YYYY-MM-DD')),
                week: dayjs().add(5, 'hour').week()
            }
        })
        const newCommand = await prisma.firePerson.create({
            data: {
                fireFireIdField: newFire.fireIdField,
                PersonCount: Math.floor(Number(newFire.forest) / 2),
                PersonType: faker.helpers.arrayElement(['Парашютно-пожарные команды', 'Рабочие лесопожарных станций', 'Работники МЧС']),
            }
        })



        return {
            location: newFire.forestName,
            number: newFire.forestFireNumber,
            state: "Продолжается",
            area: newFire.forest,
            datetimeStart: dayjs().format('YYYY-MM-DD HH:mm:ss').toString(),
            cause: newFire.cause,
            commandAmount: newCommand.PersonCount,
            commandType: newCommand.PersonType,
        }
    }
}