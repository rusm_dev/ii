import ChartJsImage from "chartjs-to-image";

const chart = new ChartJsImage();

chart.setConfig({
    type: "bubble",
    data: {
        labels: ['January', 'February', 'March', 'April', 'May'],
        datasets: [
            {
                label: 'Dogs',
                data: [50, 60, 70, 180, 190],
            },
        ],
    },
    options: {
        scales: {
            yAxes: [
                {
                    ticks: {
                        callback: function (value) {
                            return '$' + value;
                        },
                    },
                },
            ],
        },
    },
});
chart.setWidth(500).setHeight(300).setBackgroundColor('#f1f1f1');

console.log(chart.getUrl());