/*
  Warnings:

  - The `detection` column on the `Fire` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "Fire" DROP COLUMN "detection",
ADD COLUMN     "detection" TIMESTAMP(3);
