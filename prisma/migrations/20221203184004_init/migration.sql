/*
  Warnings:

  - You are about to drop the column `techniqueTip` on the `Fire` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Fire" DROP COLUMN "techniqueTip",
ADD COLUMN     "techniqueType" TEXT;
