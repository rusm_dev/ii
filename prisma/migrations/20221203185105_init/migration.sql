-- CreateTable
CREATE TABLE "FirePerson" (
    "id" SERIAL NOT NULL,
    "PersonType" TEXT,
    "PersonCount" INTEGER,

    CONSTRAINT "FirePerson_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "FireTechnique" (
    "id" SERIAL NOT NULL,
    "TechniqueType" TEXT,
    "TechniqueCount" INTEGER,
    "fireFireIdField" TEXT NOT NULL,

    CONSTRAINT "FireTechnique_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "FireTechnique" ADD CONSTRAINT "FireTechnique_fireFireIdField_fkey" FOREIGN KEY ("fireFireIdField") REFERENCES "Fire"("fireIdField") ON DELETE RESTRICT ON UPDATE CASCADE;
