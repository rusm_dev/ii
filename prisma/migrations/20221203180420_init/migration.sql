/*
  Warnings:

  - The primary key for the `Fire` table will be changed. If it partially fails, the table could be left without primary key constraint.

*/
-- AlterTable
ALTER TABLE "Fire" DROP CONSTRAINT "Fire_pkey",
ALTER COLUMN "fireIdField" DROP DEFAULT,
ALTER COLUMN "fireIdField" SET DATA TYPE TEXT,
ADD CONSTRAINT "Fire_pkey" PRIMARY KEY ("fireIdField");
DROP SEQUENCE "Fire_fireIdField_seq";
