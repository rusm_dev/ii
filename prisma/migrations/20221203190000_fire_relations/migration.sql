/*
  Warnings:

  - Added the required column `fireFireIdField` to the `FirePerson` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "FirePerson" ADD COLUMN     "fireFireIdField" TEXT NOT NULL;

-- AddForeignKey
ALTER TABLE "FirePerson" ADD CONSTRAINT "FirePerson_fireFireIdField_fkey" FOREIGN KEY ("fireFireIdField") REFERENCES "Fire"("fireIdField") ON DELETE RESTRICT ON UPDATE CASCADE;
