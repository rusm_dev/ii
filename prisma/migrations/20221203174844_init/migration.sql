/*
  Warnings:

  - You are about to drop the `Fires` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "Fires";

-- CreateTable
CREATE TABLE "Fire" (
    "fireIdField" SERIAL NOT NULL,
    "state" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "intensity" TEXT NOT NULL,
    "plants" TEXT NOT NULL,
    "mainSpecies" TEXT NOT NULL,
    "forest" INTEGER NOT NULL,
    "aviation" TEXT NOT NULL,
    "forestName" TEXT NOT NULL,
    "divisionalForest" TEXT NOT NULL,
    "forestFireNumber" TEXT NOT NULL,
    "aviationFireNumber" TEXT NOT NULL,
    "forestFundHolder" TEXT NOT NULL,
    "monitoringZone" TEXT NOT NULL,
    "latitude" TEXT NOT NULL,
    "longitude" TEXT NOT NULL,
    "municipality" TEXT NOT NULL,
    "extinctionTime" TEXT NOT NULL,
    "groundCover" TEXT NOT NULL,
    "categoryForest" TEXT NOT NULL,
    "categoryLand" TEXT NOT NULL,
    "cause" TEXT NOT NULL,
    "detectionMethod" TEXT NOT NULL,
    "peopleType" TEXT NOT NULL,
    "delivered" INTEGER NOT NULL,
    "techniqueTip" TEXT NOT NULL,
    "count" INTEGER NOT NULL,

    CONSTRAINT "Fire_pkey" PRIMARY KEY ("fireIdField")
);
