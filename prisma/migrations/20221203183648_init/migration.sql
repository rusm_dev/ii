/*
  Warnings:

  - The `forestFireNumber` column on the `Fire` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - The `aviationFireNumber` column on the `Fire` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "Fire" DROP COLUMN "forestFireNumber",
ADD COLUMN     "forestFireNumber" INTEGER,
DROP COLUMN "aviationFireNumber",
ADD COLUMN     "aviationFireNumber" INTEGER;
