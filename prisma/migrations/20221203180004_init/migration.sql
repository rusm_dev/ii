/*
  Warnings:

  - Added the required column `detection` to the `Fire` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Fire" ADD COLUMN     "detection" TEXT NOT NULL,
ALTER COLUMN "forest" SET DATA TYPE DECIMAL(65,30);
